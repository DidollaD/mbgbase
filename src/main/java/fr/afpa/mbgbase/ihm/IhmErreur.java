package fr.afpa.mbgbase.ihm;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class IhmErreur extends JFrame implements ActionListener{
	private JLabel erreurAffiche;
	private boolean erreurVis=true;
	private String nom;
	private String texteLabel;
	public IhmErreur(String texteErreur) {
		//init Fenetre
		
		setTitle("ERREUR");
		setSize(400, 150);
		BorderLayout bl= new BorderLayout();
		setLocationRelativeTo(null);
		setResizable(false);
		
		//init du Label, de sa position
		texteLabel=texteErreur;
		erreurAffiche = new JLabel(texteLabel,SwingConstants.CENTER);
		erreurAffiche.setFont(Font.getFont("Cambria"));
		
		//crea Boutons
		JButton bQuitter =new JButton("Quitter");
		bQuitter.setFont(Font.getFont("Cambria"));
		bQuitter.addActionListener(this);
		JButton bHelp =new JButton("Help");
		bHelp.setFont(Font.getFont("Cambria"));
		
		//Boutton Help
		bHelp.addActionListener(this);
		//creation d'une box pour contenir les boutons
		Box box = Box.createHorizontalBox();
		box.add(bQuitter);
		box.add(Box.createGlue());
		box.add(bHelp);
		
		//ajout des elements dans le panel
		JPanel p= new JPanel(bl);
		p.add(erreurAffiche, bl.CENTER);
		p.add(box, bl.SOUTH); 
		 
		//ajout du panel dans la fenetre
		add(p);
		setVisible(erreurVis);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if ("Help".equals(e.getActionCommand())) {
			 new IhmHelp();
		}
		else if ("Quitter".equals(e.getActionCommand())) {
			setVisible(false);
		}
		
	}

}
