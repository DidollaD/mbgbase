package fr.afpa.mbgbase.ihm;

import fr.afpa.mbgbase.control.ControlCommande;
import fr.afpa.mbgbase.entites.Table;
import fr.afpa.metiers.MetierCommandes;
import fr.afpa.metiers.MetierTable;

public class IhmMetiers {

	/**
	 * Methode qui s'occupe d'appeler le service de la creation de la base de donnée
	 * et s'occupe de l'affichage des erreurs liés à celle ci
	 * 
	 * @param commande : Commande entrée par l'utilisateur
	 * @param mC       : Instantciation de la classe MetierCommandes pour pouvoir
	 *                 utiliser les methodes de celle ci
	 */
	private void createDatabase(String commande, MetierCommandes mC) {

		if (mC.createDB(commande)) {
			IhmErreur afficherAction = new IhmErreur("Creation d'une nouvelle Base de Données reussi");
			afficherAction.setTitle("Confirmation");

		} else {
			new IhmErreur("Cette Base de Données existe déjà ou le dossier BDD a été supprimé !");
		}

	}

	/**
	 * Methode pour pouvoir créer une table dans une base de donnée specifique et
	 * pour afficher les erreurs specifiques à cette requete
	 * 
	 * @param baseDeDonneesUtilisee : l'adresse de la base de données utilisé
	 * @param commande              : la commande entrée par l'utilisateur pour
	 *                              recuperer les elements necessaires pour la
	 *                              creation de la tablee
	 */
	private void createTable(String baseDeDonneesUtilisee, String commande) {
		Table table;
		IhmErreur ihmEr;
		MetierCommandes mC = new MetierCommandes();
		MetierTable mT = new MetierTable();
		if (!ControlCommande.verificationTableExistePourCreation(baseDeDonneesUtilisee, commande)) {
			table = mC.createTab(commande);
			if (table != null) {
				mT.sauvegarde(baseDeDonneesUtilisee, table);
				ihmEr = new IhmErreur("Creation de table " + table.getName() + " reussi!");
			} else {
				ihmEr = new IhmErreur("Une Erreur est survenue !");
			}

		} else {
			ihmEr = new IhmErreur("Cette table existe déjà dans la Base de Données");
		}
	}
	/**
	 * Methode qui rassemble les methodes qui commencent par CREATE
	 * @param baseDeDonneesUtilisee : Base de donnée utilisé
	 * @param commande : commande en cours
	 */
	public void rassemblementCreate(String baseDeDonneesUtilisee, String commande) {
		MetierCommandes mC =new MetierCommandes();
		if (commande.matches("CREATE DATABASE \\w+")) {
			createDatabase(commande, mC);
		}
		else if (commande.matches("CREATE TABLE \\w+ \\([\\w,]+\\)")) {
		createTable(baseDeDonneesUtilisee, commande);
		}
		else {
			IhmErreur ihmE=new IhmErreur("Commande Invalide!");
		}
	}

}
