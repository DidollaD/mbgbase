package fr.afpa.mbgbase.ihm;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IhmHelp extends JFrame {

	private boolean helpVisible;

	public IhmHelp() {
		setHelpVisible(true);
		setTitle("HELP");
		setSize(480, 500); 
		BorderLayout br = new BorderLayout();

		String text = new String();
		text = "<html>" + "<center><b>Liste de commandes disponibles :</b></center><br>"
				+ " <font color=076EA6>USE</font> <i>maBase</i> : "
				+ " <br>Se positionner au niveau de la Base de Données <i>maBase</i>.<br><br>"
				+ " <font color=076EA6>CREATE DATABASE</font> <i>maBase</i> : "
				+ " <br>Création d'une Base de Données qui va contenir par la suite les tables.<br><br>"
				
				+ " <font color=076EA6>CREATE TABLE</font> <i>nomTable</i> <font color=076EA6>(colonne1,colonne2...)</font> : "
				+ " <br>Création de table <i>nomTable</i>.<br><br>"
				+ " <font color=076EA6>SHOW TABLES</font> : "
				+ " <br>Afficher les tables d'une Base de Données.<br><br>"
				+ " <font color=076EA6>INSERT INTO</font> <i>nomTable</i> <font color=076EA6>VALUES (valeur1,valeur2)</font> :"
				+ " <br>Insertion de données dans la table <i>nomTable</i>.<br><br>"
				+ " <font color=076EA6>SELECT</font> <i>nomChamp</i> <font color=076EA6>FROM</font> <i>nomTable</i> : "
				+ " <br>Sélection de <i>nomChamp</i> provenant de la table <i>nomTable</i>.<br><br>"
				+ " <font color=076EA6>SELECT*FROM</font> <i>nomTable</i> : <br>Sélection de tous les champs de <i>nomTable</i>.<br><br>"
				+ " <font color=076EA6>SELECT*FROM</font> <i>nomTable</i> <font color=076EA6>Order by</font> <i>nomChamp</i>  <font color=076EA6>ACS</font> ou  <font color=076EA6>DESC</font> : "
				+ " <br>Sélection de tous les champs de <i>nomTable</i> et pour les afficher <br>ordonnées par <i>nomChamp</i>.<br><br>"
				+ " <font color=076EA6>UPDATE</font> <i>nomTable</i> <font color=076EA6>SET</font> <i>nomChamp</i> ='<i>nouvelleValeur</i>' : "
				+ " <br>Attribuer une nouvelle valeur à la colonne <i>nomChamp</i> <br>pour toutes les lignes de la table <i>nomTable</i>.<br><br>"
				+ " <font color=076EA6>UPDATE</font> <i>nomTable</i> <font color=076EA6>SET</font> <i>nomChamp</i>, <i></i> ='<i>nouvelleValeur</i>'<br><font color=076EA6> WHERE</font> <i>nomChamp</i>='<i>ancienneValeur</i>' : "
				+ " <br>Attribuer une nouvelle valeur à la colonne <i>nomChamp</i> <br>pour toutes les lignes qui respectent la condition stipulée avec <font color=076EA6> WHERE</font>.<br><br>"
				+ " <font color=076EA6>DELETE FROM</font> <i>nomTable</i> : "
				+ " <br>Supprimer toutes les données de la table <i>nomTable</i>.<br><br>"
				+ " <font color=076EA6>DELETE FROM</font> <i>nomTable</i> <font color=076EA6>Where</font> <i>nomChamp</i>='<i>valeur</i>' : "
				+ " <br>Sélection de <i>nomChamp</i> provenant de la table <i>nomTable</i>.<br><br>"

				+ "</html>";

		JLabel afficheCommandes = new JLabel(text);
		JButton bouton = new JButton("Quitter");
		//ici bouton list
		afficheCommandes.setFont(Font.getFont("Cambria"));
		bouton.addActionListener(new ActionListener() { 
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				
			}
		});
		JPanel p = new JPanel(br);

		p.add(afficheCommandes, br.CENTER);
		p.add(bouton, br.SOUTH);
		

		setResizable(false);

		this.add(p);
		this.pack();
		setLocationRelativeTo(null);
		
		setVisible(helpVisible);

	}

}
