package fr.afpa.mbgbase.ihm;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import fr.afpa.metiers.MetierTable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IhmMain extends JFrame implements ActionListener{
	Container panelMain;
	Box boxAll;
	Box boxUp;
	Box boxTextareaBtn;
	Box boxBtn;
	Box boxDown;

	JMenu m1;
	JMenu m2;
	JList<String> l1;
	JTextArea ta1;
	JButton bReset;
	JButton bExe;
	JTable t1;
	/**
	 * 
	 */
	private static final long serialVersionUID = -604978157965170856L;

	public IhmMain() {
		this.setTitle("MBGB");
		panelMain = this.getContentPane();
		this.setJMenuBar(new JMenuBar());
		
		JMenuBar mb1 = this.getJMenuBar();
		//Init menu Fichier
		m1 = new JMenu("Fichier");
		
		JMenuItem mreset = new JMenuItem("Reset");
		mreset.addActionListener(this);
		m1.add(mreset);
		JMenuItem mbdd = new JMenuItem("Changer Base de Donnee");
		m1.add(mbdd);
		JMenuItem mquit = new JMenuItem("Quitter");
		mquit.addActionListener(this);
		m1.add(mquit);
		
		//Init menu Informations ou ?
		m2=new JMenu("?");
		JMenuItem info = new JMenuItem("A Propos");
		info.addActionListener(this);
		m2.add(info);
		
		JMenuItem aide = new JMenuItem("Help");
		aide.addActionListener(this);
		m2.add(aide);
		
		//init de la barre des menus
		mb1.add(m1);
		mb1.add(m2);
		
		l1 = new JList<String>();
		l1.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		l1.setLayoutOrientation(JList.VERTICAL);
		l1.setVisibleRowCount(10);
		l1.setPrototypeCellValue(String.format("%25s", ""));
		boxAll = Box.createVerticalBox();
		boxUp = Box.createHorizontalBox();
		boxTextareaBtn = Box.createVerticalBox();
		boxBtn = Box.createHorizontalBox();
		boxUp.add(new JScrollPane(l1));
		ta1 = new JTextArea(10, 20);
	
		bReset = new JButton("Reset");
		bReset.addActionListener(this);
		bExe = new JButton("Executer");
		bExe.addActionListener(this);
		boxTextareaBtn.add(ta1);
		boxBtn.add(bReset);
		boxBtn.add(bExe);
		boxTextareaBtn.add(boxBtn);
		boxUp.add(boxTextareaBtn);
		boxAll.add(boxUp);
		DefaultTableModel model = new DefaultTableModel(5, 2);
		t1 = new JTable(model);
		boxAll.add(t1);
		panelMain.add(boxAll);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setMinimumSize(new Dimension(800, 600));
		this.setSize(400, 600);
		this.setLocationRelativeTo(null);
		this.pack();
		this.setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if ("Help".equals(e.getActionCommand())) {
			 new IhmHelp();
		}
		else if ("Quitter".equals(e.getActionCommand())) {
			setVisible(false);
		}
		else if ("Reset".equals(e.getActionCommand())) {
			getTa1().setText("");
		}
		else if("A Propos".equals(e.getActionCommand())) {
			new IhmPropos();
		}
		else if("Executer".equals(e.getActionCommand())) {
			MetierTable mt = new MetierTable();
			mt.ExecuteCommande(ta1.getText(), "BTest");
		}
	}
}
