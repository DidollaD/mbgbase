package fr.afpa.mbgbase.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fr.afpa.mbgbase.control.ControlLogin;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class IhmAuthentification extends JFrame implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7011137921209158394L;
	//public boolean erreurLog = false;
	public boolean attAuthentification = true;
	private JTextField loginText;
	private JTextField passText;
	private JLabel erreur;
	public IhmAuthentification() {
	
		// Titre de la fenetre
		setTitle("Authentification");
		
		// label du login
		JLabel loginLabel = new JLabel("Login :");
		loginLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		// label du password
		JLabel passLabel = new JLabel("Passeword :");
		passLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		// box du login et du password
		Box hBox1 = Box.createVerticalBox();
		hBox1.add(loginLabel);
		hBox1.add(Box.createVerticalStrut(10));
		hBox1.add(passLabel);
		
		// champs de saisie du login
		loginText = new JTextField(10);
		loginText.setMaximumSize(loginText.getPreferredSize()); 
		loginText.addActionListener(this);
		
		// champs du password
		passText = new JTextField(10);
		passText.setMaximumSize(passText.getPreferredSize()); 
		passText.addActionListener(this);
		
		// box des champs de login et password
		Box hBox2 = Box.createVerticalBox();
		hBox2.add(loginText);
		hBox2.add(Box.createVerticalStrut(10));
		hBox2.add(passText);
		
		// box des label de logon et password et box des champs de login et password
		Box hBox3 = Box.createHorizontalBox();
		hBox3.add(hBox1);
		hBox3.add(Box.createHorizontalStrut(5));
		hBox3.add(hBox2);
		
		// label de message d'erreur de login ou passeword
		erreur = new JLabel("Le login ou le mot de pass est incorrect !");
		erreur.setForeground(new Color(255,0,0));
		erreur.setVisible(false);	
		
		// bouton de validatrion des login et mot de passe
		JButton ok = new JButton("OK");
		// action du bouton ok
		ok.addActionListener(this);
		
		// bouton d'annulation des login et mot de passe
		JButton annuler = new JButton("Annuler");
		// action du bouton annuler
		annuler.addActionListener(this);
		
		// box des bouton de validation et d'annulation
		Box hBox5 = Box.createHorizontalBox();
		hBox5.add(annuler);
		hBox5.add(Box.createHorizontalGlue());
		hBox5.add(ok);
		
		// box label et champs de saisie des login et passeword et boutton d'annulation et validation
		Box vBox = Box.createVerticalBox();
		vBox.add(hBox3);
		vBox.add(erreur);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(hBox5);
		
		
		Container c = getContentPane();
		c.add(vBox,BorderLayout.CENTER);
		// taille de la fenatre d'authentification
		setSize(400, 150);
		// interdiction de redimenssioner la fenetre
		setResizable(false);
		// centrage de la fenettre au milieu de l'ecran
		setLocationRelativeTo(null);
		// commande de fermeture de la fenetre avec la croix 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setVisible(attAuthentification);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		String login= loginText.getText();
		String pwd= passText.getText();
		ArrayList<String> listeUsers = ControlLogin.listeUsers();
		if ("OK".equals(e.getActionCommand())) {
			if (!ControlLogin.controlLogin(login)||!ControlLogin.controlPwd(pwd)) {
				erreur.setVisible(true);
			}
			if(ControlLogin.login(login, pwd, listeUsers)){
				new IhmMain();
				setVisible(false);
			}else {
				erreur.setVisible(true);
			}
			}
		if ("Annuler".equals(e.getActionCommand())) {
			System.exit(0); 
		}
		
	}
	
	
}
