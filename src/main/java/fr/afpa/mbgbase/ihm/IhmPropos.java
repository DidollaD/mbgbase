package fr.afpa.mbgbase.ihm;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.BrokenBarrierException;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class IhmPropos extends JFrame implements ActionListener{
	private boolean afficher;
	
	
	
	public IhmPropos(){
		setAfficher(true);
		setTitle("A Propos");
		setSize(480, 500); 
		BorderLayout br =new BorderLayout();
		JPanel p = new JPanel(br);
		
		String text="<html>"
				+ "<br><center><b>Projet Base de Données Réalisé par :</b></center><br>"
				+ " <center><font color=076EA6>DIDOLLA</font> David</center>"
				+ " <br><center><font color=076EA6>GRAUWIN</font> Guillaume</center> "
				+ " <br><center><font color=076EA6>ZENINA</font> Alexandra</center> "
				+ "<br></html>";
		
		JLabel label = new JLabel(text);
		Box haut = Box.createHorizontalBox();
		haut.add(label);
	
		
		
		JButton bouton = new JButton("Quitter");
		bouton.addActionListener(this);
		Box contientBouton = Box.createVerticalBox();
	
		contientBouton.add(bouton, Component.CENTER_ALIGNMENT);
	
		
		
		label.setFont(Font.getFont("Cambria"));
		
		p.add(haut, br.NORTH);
		p.add(contientBouton,br.SOUTH);
		setResizable(false);

		this.add(p);
		this.pack();
		setLocationRelativeTo(null);
		
		setVisible(afficher);
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if ("Quitter".equals(e.getActionCommand())) {
			setVisible(false);
		
	}

}
}
