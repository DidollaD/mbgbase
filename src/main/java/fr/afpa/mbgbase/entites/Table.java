package fr.afpa.mbgbase.entites;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@ToString
@AllArgsConstructor
@Getter
@Setter
public class Table implements Serializable {

	private static final long serialVersionUID = 7955747147859174223L;
	private String name;
	private ArrayList<String[]>table;

	public Table() {
		super();
		this.name=null;
		this.table = null;
	}
	 
	
	
}
