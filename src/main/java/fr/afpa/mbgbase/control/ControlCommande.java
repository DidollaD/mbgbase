package fr.afpa.mbgbase.control;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import fr.afpa.mbgbase.entites.Table;

public class ControlCommande {
	public static boolean ValideCommande(String commande) {
		boolean flag = false;
		if (commande.matches("CREATE (DATABASE \\w+|TABLE \\w+ \\([\\w,]+\\))")
				|| commande.matches("INSERT INTO \\w+ VALUES \\(('(\\w+ )*\\w+',)*'(\\w+ )*\\w+'\\)")
				|| commande.matches("SELECT (\\*|(\\w+,)*\\w+) FROM \\w+( Order by \\w+ (ASC|DESC))?")
				|| commande.matches(
						"UPDATE \\w+ SET (\\w+ = '(\\w+ )*\\w+',)*\\w+ = '(\\w+ )*\\w+'( WHERE \\w+ = '(\\w+ )*\\w+')?")
				|| commande.matches("DELETE FROM \\w+( WHERE \\w+ = '(\\w+ )*\\w+')?") || commande.matches("USE \\w+")
				|| commande.matches("SHOW TABLES"))
			flag = true;
		return flag;
	}

	public static boolean ValideParam(Table table, String commande) {
		boolean flag = false;
		String[] div0 = commande.split("\\(|\\)");
		String[] div = commande.split("\\s");
		if (commande.matches("INSERT INTO \\w+ VALUES \\(('(\\w+ )*\\w+',)*'(\\w+ )*\\w+'\\)")) {
			div = div0[1].split(",");
			if (div.length == table.getTable().get(0).length) {
				flag = true;
			}
		}
		if ("SELECT".equals(div[0])) {
			if ("*".equals(div[1])) {
				flag = true;
			} else {
				String[] div2 = div[1].split(",");
				for (int i = 0; i < table.getTable().get(0).length; i++) {
					for (int j = 0; j < div2.length; j++) {
						if (div2[j].equals(table.getTable().get(0)[i])) {
							flag = true;
						}
					}
				}
			}
			if (div.length > 4) {
				for (int i = 0; i < table.getTable().get(0).length; i++) {
					if (div[6].equals(table.getTable().get(0)[i])) {
						flag = true;
					}
				}
			}
		}
		if ("UPDATE".equals(div[0])) {
			String[] divUp = commande.split("UPDATE \\w+ SET | WHERE ");
			String[] divUp2 = divUp[1].split(" \\= |,");
			if (divUp.length == 3) {
				String divUp0 = divUp[1] + "," + divUp[2];
				divUp2 = divUp0.split(" \\= |,");
			}
			for (int i = 0; i < table.getTable().get(0).length; i++) {
				for (int j = 0; j < divUp2.length; j += 2) {
					if (divUp2[j].equals(table.getTable().get(0)[i])) {
						String value = divUp2[j + 1].substring(1, divUp2[j + 1].length() - 1);
						if (value.length() <= 25) {
							flag = true;
						}
					}
				}
			}
		}

		if ("DELETE".equals(div[0])) {
			if (div.length == 3) {
				flag = true;
			} else {
				String[] divUp = commande.split("WHERE ");
				String[] divUp2 = divUp[1].split(" \\= |,");
				if (divUp.length == 3) {
					String divUp0 = divUp[1] + "," + divUp[2];
					divUp2 = divUp0.split(" \\= |,");
				}
				for (int i = 0; i < table.getTable().get(0).length; i++) {
					for (int j = 0; j < divUp2.length; j += 2) {
						if (divUp2[j].equals(table.getTable().get(0)[i])) {
							String value = divUp2[j + 1].substring(1, divUp2[j + 1].length());
							if (value.length() <= 25) {
								flag = true;
							}
						}
					}
				}
			}

		}
		return flag;
	}

	/**
	 * Methode de controle pour verifier si le fichier existe ou pas
	 * 
	 * @param commande : Commande entrée par l'utilisateur pour recuperer le nom de
	 *                 la table et faire la verification
	 * @return boolean : Vrai si la table existe, faux dans le cas contraire
	 */
	public static boolean verificationTableExistePourCreation(String baseDeDonneesConcernee,String commande) {
		String[] recupDonnees = commande.split(" ");
		String nomTable = recupDonnees[2];
		File f = new File(System.getenv("BDD")+"\\"+baseDeDonneesConcernee +"\\"+ nomTable+".cda");
		
		return f.exists();

	}
}
