package fr.afpa.mbgbase.control;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ControlLogin {
	
	/**
	 * fonction qui verifie la saisie du login 
	 * @param login	le login de l'utilisateur
	 * @return	un boolean 
	 */
	public static boolean controlLogin(String login) {
		if (login.matches("[a-z]+")) {
			return true;
		}
		return false;
	}
	
	/**
	 *  fonction qui veriifie la saisie du mot de passe 
	 * @param pwd	le mot de passe de l'utilisateur
	 * @return	un boolean
	 */
	public static boolean controlPwd(String pwd) {
		if (pwd.matches("\\d{3}")) {
			return true;
		}
		return false;
	}
	
	/**
	 *  fonction qui verifie si les saisie de l'utilisareur peuvent lui donner accès
	 * @param login	le login de l'utilisateur
	 * @param pwd	le mot de passe de l'utilisateur
	 * @return	un boolean
	 */
	public static boolean login(String login , String pwd , ArrayList<String> lingneUser) {
		String[] loginNom = null;
		String[] pwdUser = null;
		for (int i = 0; i < lingneUser.size(); i++) {
			String[] UserLogin = lingneUser.get(i).split(";");
				for (int j = 0; j < UserLogin.length; j++) {
					if (j == 0) {
						loginNom = UserLogin[j].split(":");					
					}else if(j == 1){
						pwdUser = UserLogin[j].split(":"); 		
					}
				}
				if (login.equals(loginNom[3]) && pwd.equals(pwdUser[1])) {
					return true;
				}
		}
		return false;	
}
	/**
	 * fonction qui retourne la liste des utilisateurs
	 * @param cheminLecture	le chemin du fichier 
	 * @return	une ArrayList d'utilisateur avec login et mot de passe
	 */
	public static ArrayList<String> listeUsers(){
		ArrayList<String> lingneUser = new ArrayList<String>();
		FileReader fr=null;;
		
		try {
			fr = new FileReader(System.getenv("BDD")+"\\users.inf");
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				lingneUser.add(br.readLine());
			}
			br.close();	
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier n'existe pas");
		} catch (IOException e) {
			System.out.println("Erreur");
		}
		return lingneUser;
	}
}
