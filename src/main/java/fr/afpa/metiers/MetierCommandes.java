package fr.afpa.metiers;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.Array;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.swing.JLabel;

import fr.afpa.mbgbase.entites.Table;
import fr.afpa.mbgbase.ihm.IhmErreur;

public class MetierCommandes {
	/**
	 * Methode pour rechercher un champ entré en parametre dans la la table donnée
	 * 
	 * @param table    : Table dans la quelle on recherche le nom du champs.
	 * @param nomChamp : Le nom du champ recherchée
	 * @return un entier : Represente l'index ou se trouve le champ recherché, si le
	 *         champ est pas trouvée renvoit -1;
	 */
	public int rechercheChampDansTable(Table table, String nomChamp) {
		int result = -1;

		for (int i = 0; i < table.getTable().get(0).length; i++) {
			if (nomChamp.equals(table.getTable().get(0)[i])) {
				result = i;

			}

		}
		return result;
	}

	/**
	 * Methode pour rechercher une valeur dans un champs
	 * 
	 * @param table            : Table dans la quelle on recherche le nom du champs.
	 * @param champs           : Le nom du champ
	 * @param valeurRechercher : La valeur rechercher
	 * @return : un tableau d'entier ou les index de chaque champs sont identique a
	 *         la recherche
	 */
	public ArrayList<Integer> recharcheValeur(Table table, int champs, String valeurRechercher) {
		ArrayList<Integer> valeur = new ArrayList<Integer>();
		for (int i = 1; i < table.getTable().size(); i++) {
			if (valeurRechercher.equals(table.getTable().get(i)[champs])) {
				valeur.add(i);
			}
		}
		return valeur;
	}

	/**
	 * Methode pour créer une base de données
	 * 
	 * @param commande : on recupere la commande entrée pour la parcourir et
	 *                 recuperer le nom de la nouvelle base de donnée
	 * @return un boolean : true si l'operation a bien été effectué ou faux dans le
	 *         cas contraire pour l'affichage de l'erreur correspondante
	 */
	public boolean createDB(String commande) {
		boolean flag = false;
		String[] recupDonnees = commande.split(" ");
		String nom = recupDonnees[2];
		File f = new File(System.getenv("BDD")+"\\" + nom);
		try {
			if (f.mkdir()) {
				flag = true;

			}

		} catch (Exception e) {
		
		}
		return flag;

	}

/**
 * Methode pour créer une table dans la base de donnée selectionnée 
 * @param commande : La commande entrée pour recuperer le nom de la table et les champs de la table.
 * @return Table table: retourne la table crée avec la commande
 */
	public Table createTab(String commande) {
		MetierTable mT = new MetierTable();
		Table table = new Table();

		ArrayList<String[]> tab = new ArrayList<String[]>();
		String[] recupDonnees = commande.split(" ");
		String[] recupNomChampAvecParenth = recupDonnees[3].split("\\(");
		String[] nomChamps = recupNomChampAvecParenth[1].substring(0, recupNomChampAvecParenth[1].length() - 1)
				.split(",");
		tab.add(nomChamps);

		table.setName(recupDonnees[2]);
		table.setTable(tab);
		return table;

	}
	
	/**
	 * 	Methode qui retourne toutes les valeurs d'un champs selectionnée
	 * @param table		: Table dans la quelle on recherche les valeurs du nom du champs.
	 * @param champs	: Le nom du champs selectionnée
	 * @return			: Le tableau de valeurs attendues
	 */
	public ArrayList<String> selectChampsFromTable(Table table , String champs) {
		ArrayList<String>champsSelct = new ArrayList<String>();
			int select = rechercheChampDansTable(table, champs);
			for (int i = 0; i < table.getTable().size(); i++) {
				champsSelct.add(table.getTable().get(select)[i]);
			}
		return champsSelct;
	}


/* Methode a revoir 
	public void showTable(String cheminBaseDeDonneesUtilisee){
		File fichierBD = new File(cheminBaseDeDonneesUtilisee);
		Table table;
		ArrayList<Table> tab =new ArrayList<Table>();
		
	       for (File f : fichierBD.listFiles()) {
	    	   try {
	   			ObjectInputStream ois = new ObjectInputStream(
	   					new FileInputStream(f));
	   				table = (Table)ois.readObject();
	   				tab.add(table);
	   			ois.close();
	   			
	   		} catch (Exception e) {
	   			
	   		}
	           
	            }

  }
*/		
	
	/**
	 *  Methode qui retourne le chemin de la Base De Donnée
	 * @param nomTable	: Le nom d e la table
	 * @return	La table
	 */
	public String cheminBase( String nomBase ) {
		File chemin = new File( "C:\\ENV\\BDD\\"+nomBase);
		String cheminDeLaBase = null;
		if (chemin.isFile() && chemin.isDirectory()) {
			cheminDeLaBase = chemin.toString();
		}
		return cheminDeLaBase;
			}
	}
	


