package fr.afpa.metiers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import fr.afpa.mbgbase.control.ControlCommande;
import fr.afpa.mbgbase.entites.Table;
import fr.afpa.mbgbase.ihm.IhmMetiers;

public class MetierTable {

	/**
	 * Methode pour sauvegarder un objet de type table
	 * 
	 * @param baseD : la base de donnée dans la quelle on souhaite sauvegarder
	 * @param table : la table qu'on souhaite sauvegarger
	 * @return boolean : vrai si l'operation s'est bien effectué, si erreur renvoit
	 *         false.
	 */
	public boolean sauvegarde(String baseD, Table table) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(System.getenv("BDD") + "\\" + baseD + "\\" + table.getName() + ".cda"));
			oos.writeObject(table);
			oos.close();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * Methode pour la lecture d'une Table dans la base de données
	 * 
	 * @param baseD    : La base de données ou se trouve la table
	 * @param nomTable : La table qu'on souhaite lire
	 * @return Table : Retourne un objet de type table.
	 */
	public Table lectureTable(String baseD, String nomTable) {
		try {
			ObjectInputStream ois = new ObjectInputStream(
					new FileInputStream(System.getenv("BDD") + "\\" + baseD + "\\" + nomTable + ".cda"));
			Table table = (Table) ois.readObject();
			ois.close();
			return table;
		} catch (Exception e) {
			return null;
		}

	}

	public boolean ExecuteCommande(String commande, String baseD) {
		MetierCommandes mc = new MetierCommandes();
		IhmMetiers ihmM =new IhmMetiers();
		Table t1 = null;
		if (ControlCommande.ValideCommande(commande)) {
			String[] div = commande.split("\\s");
			switch (div[0]) {
			case "CREATE": ihmM.rassemblementCreate(baseD, commande);
			break;
			case "SELECT":
			case "DELETE":
			case "INSERT":
				t1 = lectureTable(baseD, div[2]);
				break;
			case "UPDATE":
				t1 = lectureTable(baseD, div[1]);
				break;
			default:
				break;
			}
			if (ControlCommande.ValideParam(t1, commande)) {
				if ("INSERT".equals(div[0])) {
					div = commande.split("\\(|\\)");
					String[] div2 = div[1].split(",");
					for (int i = 0; i < div2.length; i++) {
						div2[i] = div2[i].substring(1, div2[i].length() - 1);
					}
					t1.getTable().add(div2);
					
				}
				if ("UPDATE".equals(div[0])) {
					String[] divUp = commande.split("UPDATE \\w+ SET | WHERE ");
					String[] divUp2 = divUp[1].split(" \\= |,");
					if (divUp.length == 2) {
						
						for (int i = 0; i < divUp2.length; i+=2) {
							int id =  mc.rechercheChampDansTable(t1, divUp2[i]);
							for (int j = 1;j < t1.getTable().size();j++) {
								t1.getTable().get(j)[id] = divUp2[i+1].substring(1, divUp2[i+1].length() - 1);
							}
						}	
					}
				}
				if ("DELETE".equals(div[0])) {
					if (div.length == 3) {
						int j = t1.getTable().size();
						for (int i = 1; i < j; i++) {
							t1.getTable().remove(1);
						}
					}
				}
			}
		}
		sauvegarde(baseD, t1);
		return true;
	}
}
