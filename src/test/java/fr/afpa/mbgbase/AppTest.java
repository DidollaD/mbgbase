package fr.afpa.mbgbase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.afpa.mbgbase.control.ControlCommande;
import fr.afpa.mbgbase.entites.Table;
import fr.afpa.metiers.MetierCommandes;
import fr.afpa.metiers.MetierTable;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
	@Test
	public void testValideCommande() {
		assertTrue(ControlCommande.ValideCommande("CREATE TABLE nomTable (colonne1,colonne2,colonne3)"));
		assertTrue(ControlCommande.ValideCommande("INSERT INTO nomTable VALUES ('valeur1','valeur2')"));
		assertTrue(ControlCommande.ValideCommande("SELECT * FROM nomTable"));
		assertTrue(ControlCommande.ValideCommande("SELECT nom_du_champ FROM nomTable"));
		assertTrue(ControlCommande.ValideCommande("SELECT * FROM nomTable Order by nom_du_champ ASC"));
		assertTrue(ControlCommande.ValideCommande("SELECT * FROM nomTable Order by nom_du_champ DESC"));
		assertTrue(ControlCommande.ValideCommande("UPDATE nomTable SET nom_du_champ = 'nouvelle Valeur'"));
		assertTrue(ControlCommande.ValideCommande("UPDATE nomTable SET nom_du_champ = 'nouvelleValeur',nom_du_champ1 = 'nouvelle valeur1'"));
		assertTrue(ControlCommande.ValideCommande("UPDATE nomTable SET nom_du_champ = 'nouvelle valeur' WHERE nom_du_champ = 'ancienne valeur'"));
		assertTrue(ControlCommande.ValideCommande("DELETE FROM nomTable"));
		assertTrue(ControlCommande.ValideCommande("DELETE FROM nomTable WHERE nom_du_champ = 'valeur'"));
		assertTrue(ControlCommande.ValideCommande("CREATE DATABASE ma_base"));
		assertTrue(ControlCommande.ValideCommande("USE ma_base"));
		assertTrue(ControlCommande.ValideCommande("SHOW TABLES"));
	}
	
	@Test
	public void testSauvegarde() {
		MetierTable mt = new MetierTable();
		 ArrayList<String []> a = new ArrayList<>();
	      String b[] = {"Nom","Prenom"};
	      String c[] = {"Sacha","Zen"};
	      a.add(b);
	      a.add(c);
	      Table table= new Table();
	      table.setName("Test");
	      table.setTable(a);
	      
		assertTrue(mt.sauvegarde("BTest", table));}
	
	@Test
	public void testLecture() {
		MetierTable mt = new MetierTable();
		assertNotNull(mt.lectureTable("BTest", "Test"));
	}
	
	@Test
	public void testValideParam() {
		ArrayList<String[]> tab = new ArrayList<String[]>();
		tab.add(new String[]{"nom","prenom"});
		tab.add(new String[]{"Didolla","David"});
		tab.add(new String[]{"Grauwin","Guigaum"});
		Table table = new Table("personne",tab);
		assertTrue(ControlCommande.ValideParam(table,"INSERT INTO nomTable VALUES ('valeur1','valeur2')"));
		assertTrue(ControlCommande.ValideParam(table,"SELECT * FROM personne"));	
		assertTrue(ControlCommande.ValideParam(table,"SELECT nom FROM personne"));
		assertTrue(ControlCommande.ValideParam(table,"SELECT nom,prenom FROM personne"));
		assertTrue(ControlCommande.ValideParam(table,"SELECT * FROM nomTable Order by nom_du_champ ASC"));
		assertTrue(ControlCommande.ValideParam(table,"SELECT nom FROM nomTable Order by nom_du_champ ASC"));
		assertTrue(ControlCommande.ValideParam(table,"SELECT nom,prenom FROM nomTable Order by nom_du_champ ASC"));
		assertTrue(ControlCommande.ValideParam(table,"UPDATE nomTable SET nom = 'nouvelle Valeur'"));
		assertTrue(ControlCommande.ValideParam(table,"UPDATE nomTable SET nom = 'nouvelleValeur',prenom = 'nouvelle valeur1'"));
		assertTrue(ControlCommande.ValideParam(table,"UPDATE nomTable SET nom = 'nouvelle valeur' WHERE nom = 'ancienne valeur'"));
		assertTrue(ControlCommande.ValideParam(table,"DELETE FROM nomTable"));
		assertTrue(ControlCommande.ValideParam(table,"DELETE FROM nomTable WHERE nom = 'valeur'"));
	}
	
	@Test
	public void testExecuteCommande() {
		MetierTable mt = new MetierTable();
		assertTrue((mt.ExecuteCommande("INSERT INTO TestInto VALUES ('nom','prenom')","BTest")));
		assertTrue((mt.ExecuteCommande("UPDATE TestInto SET Nom = 'Didier'","BTest")));
		assertTrue((mt.ExecuteCommande("UPDATE TestInto SET Nom = 'Didolla',Prenom = 'David'","BTest")));
		assertTrue((mt.ExecuteCommande("DELETE FROM TestInto","BTest")));
	}
	

@Test
public void testRechercheChampDansTable() {
	MetierCommandes mC =new MetierCommandes();
	 ArrayList<String []> a = new ArrayList<>();
     String b[] = {"Nom","Prenom"};
     a.add(b);
     Table table= new Table();
     table.setName("Test");
     table.setTable(a);
     
     assertEquals(1, mC.rechercheChampDansTable(table, "Prenom"));
	
}

@Test 
public void testCreateDB() {
MetierCommandes mC =new MetierCommandes();
assertFalse(mC.createDB("CREATE DATABASE TestCreationBase"));
}

@Test
public void testVerificationTableExistePourCreation() {
		assertTrue(ControlCommande.verificationTableExistePourCreation("BTest", "CREATE TABLE Test"));
}
}
